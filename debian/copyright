Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mupen64plus-video-glide64mk2
Upstream-Contact: mupen64plus@googlegroups.com
Source: https://github.com/mupen64plus/mupen64plus-video-glide64mk2/

Files: *
Copyright: 2003-2009, Sergey 'Gonetz' Lipski
  2002, Dave2001 <Dave2999@hotmail.com>
License: GPL-2+

Files: debian/*
Copyright: 2009-2022, Sven Eckelmann <sven@narfation.org>
  2009-2022, Tobias Loose <TobiasLoose@gmx.de>
License: GPL-2+

Files: projects/unix/Makefile
Copyright: 2007-2008, Jesse 'DarkJezter' Dean
  2007-2008, Scott 'Tillin9' Knauert
  2007-2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
  2010, Jon 'wahrhaft' Ring <wahrh4ft@gmail.com>
License: GPL-2+

Files: src/Glide64/Config.cpp
  src/Glide64/Config.h
Copyright: 2010, Jon 'wahrhaft' Ring <wahrh4ft@gmail.com>
  2002, Dave2001 <Dave2999@hotmail.com>
License: GPL-2+

Files: src/Glide64/FrameSkipper.cpp
  src/Glide64/FrameSkipper.h
Copyright: 2011, yongzh <freeman.yong@gmail.com>
License: GPL-2+

Files: src/Glide64/Ini.cpp
  src/Glide64/Ini.h
Copyright: 2002, Dave2001 <Dave2999@hotmail.com>
License: GPL-2+

Files: src/Glide64/m64p.h
Copyright: 2010, Jon 'wahrhaft' Ring <wahrh4ft@gmail.com>
License: GPL-2+

Files: src/Glide64/osal_dynamiclib.h
  src/Glide64/osal_dynamiclib_unix.c
  src/Glide64/osal_dynamiclib_win32.c
  src/GlideHQ/osal_files.h
  src/GlideHQ/osal_files_win32.c
  src/GlideHQ/osal_files_unix.c
Copyright: 2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: src/Glide64/winlnxdefs.h
Copyright: 2012, 2013, balrog
  2012, 2013, wahrhaft
  2003-2009, Sergey 'Gonetz' Lipski
  2002, Dave2001 <Dave2999@hotmail.com>
License: GPL-2+

Files: src/GlideHQ/*
Copyright: 2007, Hiroshi Morii <koolsmoky@users.sourceforge.net>
License: GPL-2+

Files: src/GlideHQ/TextureFilters.cpp
  src/GlideHQ/TextureFilters_hq2x.cpp
  src/GlideHQ/TextureFilters_hq2x.h
  src/GlideHQ/TextureFilters_lq2x.h
Copyright: 2007, Hiroshi Morii <koolsmoky@users.sourceforge.net>
  2003, Rice1964
License: GPL-2+

Files: src/GlideHQ/tc-1.1+/*
Copyright: 2004, Daniel Borca
License: GPL-2+

Files: src/GlideHQ/tc-1.1+/fxt1.c
  src/GlideHQ/tc-1.1+/texstore.c
Copyright: 1999-2008, Brian Paul
License: Expat

Files: src/GlideHQ/tc-1.1+/fxt1.h
Copyright: 2004, Daniel Borca
License: Expat

Files: src/GlideHQ/tc-1.1+/s2tc/*
Copyright: 2011, Rudolf Polzer
License: Expat

Files: src/GlideHQ/tc-1.1+/s2tc/txc_dxtn.h
Copyright: 2004, Roland Scheidegger
License: Expat

Files: src/Glitch64/*
Copyright: 2007, Hiroshi 'KoolSmoky' Morii <koolsmoky@users.sourceforge.net>
License: GPL-2+

Files: src/Glitch64/inc/*
Copyright: 1999, 3DFX INTERACTIVE, INC.
License: other
 3DFX GLIDE Source Code General Public License
 .
 1. PREAMBLE
 .
        This license is for software that provides a 3D graphics application
        program interface (API).The license is intended to offer terms similar
        to some standard General Public Licenses designed to foster open
        standards and unrestricted accessibility to source code. Some of these
        licenses require that, as a condition of the license of the software,
        any derivative works (that is, new software which is a work containing
        the original program or a portion of it) must be available for general
        use, without restriction other than for a minor transfer fee, and that
        the source code for such derivative works must likewise be made
        available. The only restriction is that such derivative works must be
        subject to the same General Public License terms as the original work.
 .
        This 3dfx GLIDE Source Code General Public License differs from the
        standard licenses of this type in that it does not require the entire
        derivative work to be made available under the terms of this license
        nor is the recipient required to make available the source code for
        the entire derivative work. Rather, the license is limited to only the
        identifiable portion of the derivative work that is derived from the
        licensed software. The precise terms and conditions for copying,
        distribution and modification follow.
 .
 2. DEFINITIONS
 .
        2.1 This License applies to any program (or other "work") which
        contains a notice placed by the copyright holder saying it may be
        distributed under the terms of this 3dfx GLIDE Source Code General
        Public License.
 .
        2.2 The term "Program" as used in this Agreement refers to 3DFX's
        GLIDE source code and object code and any Derivative Work.
 .
        2.3 "Derivative Work" means, for the purpose of the License, that
        portion of any work that contains the Program or the identifiable
        portion of a work that is derived from the Program, either verbatim or
        with modifications and/or translated into another language, and that
        performs 3D graphics API operations. It does not include any other
        portions of a work.
 .
        2.4 "Modifications of the Program" means any work, which includes a
        Derivative Work, and includes the whole of such work.
 .
        2.5 "License" means this 3dfx GLIDE Source Code General Public License.
 .
        2.6 The "Source Code" for a work means the preferred form of the work
        for making modifications to it. For an executable work, complete source
        code means all the source code for all modules it contains, any
        associated interface definition files, and the scripts used to control
        compilation and installation of the executable work.
 .
        2.7 "3dfx" means 3dfx Interactive, Inc.
 .
 3. LICENSED ACTIVITIES
 .
        3.1 COPYING - You may copy and distribute verbatim copies of the
        Program's Source Code as you receive it, in any medium, subject to the
        provision of section 3.3 and provided also that:
 .
                (a) you conspicuously and appropriately publish on each copy
        an appropriate copyright notice (3dfx Interactive, Inc. 1999), a notice
        that recipients who wish to copy, distribute or modify the Program can
        only do so subject to this License, and a disclaimer of warranty as
        set forth in section 5;
 .
                (b) keep intact all the notices that refer to this License and
        to the absence of any warranty; and
 .
                (c) do not make any use of the GLIDE trademark without the prior
        written permission of 3dfx, and
 .
                (d) give all recipients of the Program a copy of this License
        along with the Program or instructions on how to easily receive a copy
        of this License.
 .
        3.2 MODIFICATION OF THE PROGRAM/DERIVATIVE WORKS - You may modify your
        copy or copies of the Program or any portion of it, and copy and
        distribute such modifications subject to the provisions of section 3.3
        and provided that you also meet all of the following conditions:
 .
                (a) you conspicuously and appropriately publish on each copy
        of a Derivative Work an appropriate copyright notice, a notice that
        recipients who wish to copy, distribute or modify the Derivative Work
        can only do so subject to this License, and a disclaimer of warranty
        as set forth in section 5;
 .
                (b) keep intact all the notices that refer to this License and
        to the absence of any warranty; and
 .
                (c) give all recipients of the Derivative Work a copy of this
        License along with the Derivative Work or instructions on how to easily
        receive a copy of this License.
 .
                (d) You must cause the modified files of the Derivative Work
        to carry prominent notices stating that you changed the files and the
        date of any change.
 .
                (e) You must cause any Derivative Work that you distribute or
        publish to be licensed at no charge to all third parties under the
        terms of this License.
 .
                (f) You do not make any use of the GLIDE trademark without the
        prior written permission of 3dfx.
 .
                (g) If the Derivative Work normally reads commands
        interactively when run, you must cause it, when started running for
        such interactive use, to print or display an announcement as follows:
 .
        "COPYRIGHT 3DFX INTERACTIVE, INC. 1999, ALL RIGHTS RESERVED THIS
        SOFTWARE IS FREE AND PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND,
        EITHER EXPRESSED OR IMPLIED. THERE IS NO RIGHT TO USE THE GLIDE
        TRADEMARK WITHOUT PRIOR WRITTEN PERMISSION OF 3DFX INTERACTIVE,
        INC. SEE THE 3DFX GLIDE GENERAL PUBLIC LICENSE FOR A FULL TEXT OF THE
        DISTRIBUTION AND NON-WARRANTY PROVISIONS (REQUEST COPY FROM
        INFO@3DFX.COM)."
 .
                (h) The requirements of this section 3.2 do not apply to the
        modified work as a whole but only to the Derivative Work. It is not
        the intent of this License to claim rights or contest your rights to
        work written entirely by you; rather, the intent is to exercise the
        right to control the distribution of Derivative Works.
 .
        3.3 DISTRIBUTION
 .
                (a) All copies of the Program or Derivative Works which are
        distributed must include in the file headers the following language
        verbatim:
 .
        "THIS SOFTWARE IS SUBJECT TO COPYRIGHT PROTECTION AND IS OFFERED
        ONLY PURSUANT TO THE 3DFX GLIDE GENERAL PUBLIC LICENSE. THERE IS NO
        RIGHT TO USE THE GLIDE TRADEMARK WITHOUT PRIOR WRITTEN PERMISSION OF
        3DFX INTERACTIVE, INC. A COPY OF THIS LICENSE MAY BE OBTAINED FROM
        THE DISTRIBUTOR OR BY CONTACTING 3DFX INTERACTIVE INC (info@3dfx.com).
        THIS PROGRAM. IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
        EXPRESSED OR IMPLIED. SEE THE 3DFX GLIDE GENERAL PUBLIC LICENSE FOR A
        FULL TEXT OF THE NON-WARRANTY PROVISIONS.
 .
        USE, DUPLICATION OR DISCLOSURE BY THE GOVERNMENT IS SUBJECT TO
        RESTRICTIONS AS SET FORTH IN SUBDIVISION (C)(1)(II) OF THE RIGHTS
        IN TECHNICAL DATA AND COMPUTER SOFTWARE CLAUSE AT DFARS 252.227-7013,
        AND/OR IN SIMILAR OR SUCCESSOR CLAUSES IN THE FAR, DOD OR NASA FAR
        SUPPLEMENT. UNPUBLISHED RIGHTS RESERVED UNDER THE COPYRIGHT LAWS OF
        THE UNITED STATES.
 .
        COPYRIGHT 3DFX INTERACTIVE, INC. 1999, ALL RIGHTS RESERVED"
 .
                (b) You may distribute the Program or a Derivative Work in
        object code or executable form under the terms of Sections 3.1 and 3.2
        provided that you also do one of the following:
 .
                        (1) Accompany it with the complete corresponding
        machine-readable source code, which must be distributed under the
        terms of Sections 3.1 and 3.2; or,
 .
                        (2) Accompany it with a written offer, valid for at
        least three years, to give any third party, for a charge no more than
        your cost of physically performing source distribution, a complete
        machine-readable copy of the corresponding source code, to be
        distributed under the terms of Sections 3.1 and 3.2 on a medium
        customarily used for software interchange; or,
 .
                        (3) Accompany it with the information you received as
        to the offer to distribute corresponding source code. (This alternative
        is allowed only for noncommercial distribution and only if you received
        the program in object code or executable form with such an offer, in
        accord with Subsection 3.3(b)(2) above.)
 .
                (c) The source code distributed need not include anything
        that is normally distributed (in either source or binary form) with
        the major components (compiler, kernel, and so on) of the operating
        system on which the executable runs, unless that component itself
        accompanies the executable code.
 .
                (d) If distribution of executable code or object code is made
        by offering access to copy from a designated place, then offering
        equivalent access to copy the source code from the same place counts
        as distribution of the source code, even though third parties are not
        compelled to copy the source along with the object code.
 .
                (e) Each time you redistribute the Program or any Derivative
        Work, the recipient automatically receives a license from 3dfx and
        successor licensors to copy, distribute or modify the Program and
        Derivative Works subject to the terms and conditions of the License.
        You may not impose any further restrictions on the recipients'
        exercise of the rights granted herein. You are not responsible for
        enforcing compliance by third parties to this License.
 .
                (f) You may not make any use of the GLIDE trademark without
        the prior written permission of 3dfx.
 .
                (g) You may not copy, modify, sublicense, or distribute the
        Program or any Derivative Works except as expressly provided under
        this License. Any attempt otherwise to copy, modify, sublicense or
        distribute the Program or any Derivative Works is void, and will
        automatically terminate your rights under this License. However,
        parties who have received copies, or rights, from you under this
        License will not have their licenses terminated so long as such
        parties remain in full compliance.
 .
 4. MISCELLANEOUS
 .
        4.1 Acceptance of this License is voluntary. By using, modifying or
        distributing the Program or any Derivative Work, you indicate your
        acceptance of this License to do so, and all its terms and conditions
        for copying, distributing or modifying the Program or works based on
        it. Nothing else grants you permission to modify or distribute the
        Program or Derivative Works and doing so without acceptance of this
        License is in violation of the U.S. and international copyright laws.
 .
        4.2 If the distribution and/or use of the Program or Derivative Works
        is restricted in certain countries either by patents or by copyrighted
        interfaces, the original copyright holder who places the Program under
        this License may add an explicit geographical distribution limitation
        excluding those countries, so that distribution is permitted only in
        or among countries not thus excluded. In such case, this License
        incorporates the limitation as if written in the body of this License.
 .
        4.3 This License is to be construed according to the laws of the
        State of California and you consent to personal jurisdiction in the
        State of California in the event it is necessary to enforce the
        provisions of this License.
 .
 5. NO WARRANTIES
 .
        5.1 TO THE EXTENT PERMITTED BY APPLICABLE LAW, THERE IS NO WARRANTY
        FOR THE PROGRAM. OR DERIVATIVE WORKS THE COPYRIGHT HOLDERS AND/OR
        OTHER PARTIES PROVIDE THE PROGRAM AND ANY DERIVATIVE WORKS"AS IS"
        WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
        BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
        FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY
        AND PERFORMANCE OF THE PROGRAM AND ANY DERIVATIVE WORK IS WITH YOU.
        SHOULD THE PROGRAM OR ANY DERIVATIVE WORK PROVE DEFECTIVE, YOU ASSUME
        THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 .
        5.2 IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW WILL 3DFX
        INTERACTIVE, INC., OR ANY OTHER COPYRIGHT HOLDER, OR ANY OTHER PARTY
        WHO MAY MODIFY AND/OR REDISTRIBUTE THE PROGRAM OR DERIVATIVE WORKS AS
        PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL,
        SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR
        INABILITY TO USE THE PROGRAM OR DERIVATIVE WORKS (INCLUDING BUT NOT
        LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES
        SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM OR
        DERIVATIVE WORKS TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH
        HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
        DAMAGES.

Files: src/Glitch64/m64p.h
Copyright: 2010, Jon 'wahrhaft' Ring <wahrh4ft@gmail.com>
License: GPL-2+

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.
